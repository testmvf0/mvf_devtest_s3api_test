# MVF Developer Tests
## S3 API

Thanks for your interest in a developer role at MVF.

We have a simple project which we would like you to take a look at in your own time.

The task is to build a RESTful API based on data we have in our AWS S3 bucket.
You can attempt this in any language you are familiar with, using any libraries, modules or frameworks you feel appropriate.
See it as a opportunity to showcase your professional software engineering skills.

Please use git to develop your submission, and commit regularly without squashing or rebasing.
This is so that we can see how you work and go about solving problems.
Please submit a public link to your repository if you can.

How would you implement this? Fork our repo and let us see your ideas!

### Challenge
Build an API to implement some of the user stories in "userStories.md".

We have a public S3 bucket set up at: https://mvf-devtest-s3api.s3-eu-west-1.amazonaws.com/

This contains a number of json files named with the pattern "<guid>.json", e.g.:
    https://mvf-devtest-s3api.s3-eu-west-1.amazonaws.com/a4a06bb0-3fbe-40bd-9db2-f68354ba742f.json

Each guid (globally unique identifier) represents a customer with access to the API and the data listed within the file represent the accounts they have access to, each of these accounts also being identified by a guid.

Your API should expose the data according to the following rules:

  1. Users with knowledge of a customer guid (Customers) may search the json file named with the guid and may have access to all it's contents.
  2. Users with knowledge of an account guid (Account Holders) may have access to any of the data for that account, but not anything else within the same file.


### Deliverables

Use Git.

Decide which stories from "userStories.md" to implement.
Unless we have asked you to look at particular stories, there is no minimum number, or correct order to them.
Please pick stories that you think will best demonstrate your skills.

Document

  1. Which stories you have completed.
  2. How to run or serve your app.
  3. How to call your endpoints.

---
### MVF
Do you want to work with the Smartest Tech and the Sharpest Minds? Apply at: http://www.mvfglobal.com/vacancies